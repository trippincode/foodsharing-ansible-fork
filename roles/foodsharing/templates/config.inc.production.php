<?php
// ## PRODUCTION CONFIG ##
if(!defined('ROOT_DIR'))
{
	define('ROOT_DIR', './');
}
define('ERROR_REPORT',E_ALL);

// # Site
define('SITE_ENVIRONMENT', 'production');

// # Database
define('DB_HOST', '{{ mysql_bind_address | default("localhost") }}');
define('DB_USER', '{{ fs_database_name }}');
define('DB_PASS', '{{ mysql_fsprod_password }}');
define('DB_DB', '{{ fs_database_name }}');

define('MEM_ENABLED', true);
define('REDIS_HOST', '127.0.0.1');
define('REDIS_PORT', '6379');

// # URLs
define('BASE_URL', 'https://foodsharing.de');
define('WEBCAL_URL', 'webcal://foodsharing.de');
define('URL_INTERN', 'https://foodsharing.de');
define('LINK_URL', 'https://foodsharing.de/');

// # Mail
define('DEFAULT_EMAIL', 'no-reply@foodsharing.network');
define('SUPPORT_EMAIL', 'it@foodsharing.network');
define('DEFAULT_EMAIL_NAME', 'Foodsharing');
define('EMAIL_PUBLIC', 'info@foodsharing.de');
define('EMAIL_PUBLIC_NAME', 'Foodsharing');
define('NOREPLY_EMAIL_HOST', 'foodsharing.de');
define('PLATFORM_MAILBOX_HOST', 'foodsharing.network');
define('DEFAULT_HOST', 'foodsharing.de');
define('DEFAULT_EMAIL_HOST', 'foodsharing.de');
define('MAILBOX_OWN_DOMAINS', ['foodsharing.network']);
define('IMAP', [
    ['host' => 'imap.mailbox.org', 'user' => 'catchall@foodsharing.network', 'password' => '{{ catchall_imap_pass }}']
]);
define('IMAP_FAILED_BOX', '{{ imap_failed_box }}');
define('BOUNCE_IMAP_HOST', 'imap.mailbox.org');
define('BOUNCE_IMAP_USER', 'no-reply@foodsharing.network');
define('BOUNCE_IMAP_PASS', '{{ bounce_imap_pass }}');
define('BOUNCE_IMAP_PORT', '993');
define('BOUNCE_IMAP_SERVICE_OPTION', 'ssl');
define('BOUNCE_IMAP_UNPROCESSED_BOX', '{{ bounce_imap_unprocessed_box }}');
define('MAILER_HOST', 'smtp://localhost');
define('DELAY_MICRO_SECONDS_BETWEEN_MAILS', '{{ fs_mail_delay }}');

// # Push Notification
define('FCM_KEY', '{{ google_fcm_key }}');
/*
 * For generating the keys see:
 *  https://github.com/web-push-libs/web-push-php#authentication-vapid
 */
define('WEBPUSH_PUBLIC_KEY', '{{ webpush_public_key }}');
define('WEBPUSH_PRIVATE_KEY', '{{ webpush_private_key }}');

// # Twingle URL for caching donation campaign
define('TWINGLE_URL', '{{ twingle_donation_status_url }}');

// # Geoapify map tiles
define('GEOAPIFY_API_KEY', '62e5c7dd44b64591b3f9d2ce4bea68d5');

// # Monitoring and Reporting
define('SENTRY_URL', '{{ senrty_url_prod }}');
define('SENTRY_TRACING_SAMPLE_RATE', 0.04);
define('RAVEN_JAVASCRIPT_CONFIG', '{{ sentry_raven_javascript_config_prod }}');

// CSP reports are too noisy so they are disabled now
// define('CSP_REPORT_URI', '{{ sentry_csp_report_prod }}');
define('CSP_REPORT_ONLY', false);


// # BigBlueButton
define('BBB_DOMAIN', 'bbb.foodsharing.network');
define('BBB_SECRET', '{{ bbb_secret }}');
define('BBB_DIALIN', '+49 5563 263 9980 (Deutschland) oder +49 3425 9999 000 (Deutschland) oder +41 3251 23 23 0 (Schweiz)');

/*
 * the following cache settings are an _example_.
 * Caching should have no or only minimum impact on the displayed content (e.g. some seconds delay/ttl is acceptable for most content, but should only be used when there is a reason).
 * caching indexes:
* u = cache page for logged in users
* g = cache page for guest
* value is time to live for the cache...
*/
$both = array('u' => 3600, 'g' => 3600);
$user = array('u' => 3600);
$guests = array('g' => 3600);
$both_short = array('u' => 60, 'g' => 60);
$user_short = array('u' => 60);

$g_page_cache = array(
    '/' => $both_short,
		'/?page=map' => $user,
		'/?page=map&load=baskets' => array('g' => 120, 'u' => 120),
		'/?page=map$load=fairteiler' => $both,
		//'/?page=dashboard' => $user,
		'/api/map/markers?types%5B%5D=baskets' => array('u' => 120, 'g' => 120),
		'/api/map/markers?types%5B%5D=' => $both,
		'/api/map/markers?types%5B%5D=baskets&types%5B%5D=fairteiler' => array('u' => 120, 'g' => 120),
		'/api/map/markers?types%5B%5D=fairteiler' => $both,
		'/api/map/markers?types%5B%5D=betriebe&options%5B%5D=needhelp&options%5B%5D=needhelpinstant' => $user,
		'/api/map/markers?types%5B%5D=botschafter' => $user,
		'/api/map/markers?types%5B%5D=foodsaver' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=botschafter' => $user,
		'/api/map/markers?types%5B%5D=botschafter&types%5B%5D=betriebe&options%5B%5D=needhelp&options%5B%5D=needhelpinstant' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=betriebe&options%5B%5D=needhelp&options%5B%5D=needhelpinstant' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=betriebe&options%5B%5D=allebetriebe' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=botschafter&types%5B%5D=betriebe&options%5B%5D=allebetriebe' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=botschafter&types%5B%5D=betriebe&types%5B%5D=fairteiler&options%5B%5D=allebetriebe' => $user,
		'/api/map/markers?types%5B%5D=betriebe&types%5B%5D=fairteiler&options%5B%5D=needhelp&options%5B%5D=needhelpinstant' => $user,
		'/api/map/markers?types%5B%5D=botschafter&types%5B%5D=fairteiler' => $user,
		'/api/map/markers?types%5B%5D=foodsaver&types%5B%5D=fairteiler' => $user
);
