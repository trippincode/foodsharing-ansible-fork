---
- name: "Add sftp user"
  user:
    name: "{{ upload_username }}"
    shell: /bin/false

- name: "Change home permissions for sftp chroot"
  file:
    path: /home/{{ upload_username }}
    owner: root
    group: "{{ upload_username }}"
    mode: 0750
    state: directory

- name: "Create upload sync folder"
  file:
    path: /home/{{ upload_username }}/uploads
    owner: "{{ upload_username }}"
    group: "{{ upload_username }}"
    state: directory

- name: "Create Authorized Keys folder folder"
  file:
    path: /etc/ssh/authorized_keys
    state: directory

- name: Match User configuration for upload
  copy:
    dest: /etc/ssh/sshd_config.d/upload.conf
    content: |
      Match User {{ upload_username }}
        ChrootDirectory /home/{{ upload_username }}
        AuthorizedKeysFile /etc/ssh/authorized_keys/{{ upload_username }}
        ForceCommand internal-sftp
        AllowTcpForwarding no
        X11Forwarding no
        PasswordAuthentication no
  notify: restart sshd
  when: ansible_lsb.release > "10"

- name: Match User configuration for upload
  blockinfile:
    path: /etc/ssh/sshd_config
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ upload_username }}"
    block: |
      Match User {{ upload_username }}
        ChrootDirectory /home/{{ upload_username }}
        AuthorizedKeysFile /etc/ssh/authorized_keys/{{ upload_username }}
        ForceCommand internal-sftp
        AllowTcpForwarding no
        X11Forwarding no
        PasswordAuthentication no
  notify: restart sshd
  when: ansible_lsb.release == "10"

- name: Copy README
  copy:
    dest: /home/{{ upload_username }}/README
    owner: "{{ upload_username }}"
    group: "{{ upload_username }}"
    content: |
      Put files you want to make available into the uploads folder.
      
      You will need to wait 5 Minutes until you can access the files.
      
      They will appear at https://foodsharing.de/uploads/<filename>
      
      Please remove unused files.

- name: "add keys for upload"
  authorized_key:
    user: "{{ upload_username }}"
    path: /etc/ssh/authorized_keys/{{ upload_username }}
    state: present
    key: "{{ lookup('file', item) }}"
  with_fileglob:
    - pub_keys/upload/*

- name: foodsharing sync uploads folder cronjob
  import_role:
    name: systemd_job
  vars:
    job_name: fs-uploads-sync
    job_description: foodsharing - sync uploads folder every 5 minutes
    job: bash -c 'rsync -ai --delete --backup --backup-dir=/home/{{ upload_username }}/.backup/$(date --iso-8601) --chown={{ fs_php_username }}:www-data --chmod=D750,F640 /home/{{ upload_username }}/uploads/ {{ uploads_directory }}/'
    job_timer_onboot: 300
    job_timer_active: 300
    job_accuracy: 10
