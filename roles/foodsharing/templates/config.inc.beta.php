<?php
// ## BETA CONFIG ##
if(!defined('ROOT_DIR'))
{
	define('ROOT_DIR', './');
}
define('ERROR_REPORT',E_ALL);

// # Site
define('SITE_ENVIRONMENT', 'beta');

// # Database
define('DB_HOST', '{{ mysql_bind_address | default("localhost") }}');
define('DB_USER', '{{ fs_database_name }}');
define('DB_PASS', '{{ mysql_fsprod_password }}');
define('DB_DB', '{{ fs_database_name }}');

define('MEM_ENABLED', true);
define('REDIS_HOST', '127.0.0.1');
define('REDIS_PORT', '6379');

// # URLs
define('BASE_URL', 'https://foodsharing.de');
define('WEBCAL_URL', 'webcal://beta.foodsharing.de');
define('URL_INTERN', 'https://foodsharing.de');
define('LINK_URL', 'https://foodsharing.de/');

// # Mail
define('DEFAULT_EMAIL', 'no-reply@foodsharing.network');
define('SUPPORT_EMAIL', 'it@foodsharing.network');
define('DEFAULT_EMAIL_NAME', 'Foodsharing');
define('EMAIL_PUBLIC', 'info@foodsharing.de');
define('EMAIL_PUBLIC_NAME', 'Foodsharing');
define('NOREPLY_EMAIL_HOST', 'foodsharing.de');
define('PLATFORM_MAILBOX_HOST', 'foodsharing.network');
define('DEFAULT_HOST', 'foodsharing.de');
define('DEFAULT_EMAIL_HOST', 'foodsharing.de');
define('MAILBOX_OWN_DOMAINS', ['foodsharing.network']);
define('IMAP', [
    ['host' => 'imap.mailbox.org', 'user' => 'catchall@foodsharing.network', 'password' => '{{ catchall_imap_pass }}']
]);
define('IMAP_FAILED_BOX', '{{ imap_failed_box }}');
define('BOUNCE_IMAP_HOST', 'imap.mailbox.org');
define('BOUNCE_IMAP_USER', 'no-reply@foodsharing.network');
define('BOUNCE_IMAP_PASS', '{{ bounce_imap_pass }}');
define('BOUNCE_IMAP_PORT', '993');
define('BOUNCE_IMAP_SERVICE_OPTION', 'ssl');
define('BOUNCE_IMAP_UNPROCESSED_BOX', '{{ bounce_imap_unprocessed_box }}');
define('MAILER_HOST', 'smtp://localhost');
define('DELAY_MICRO_SECONDS_BETWEEN_MAILS', '{{ fs_mail_delay }}');

// # Push Notification
define('FCM_KEY', '{{ google_fcm_key }}');
/*
 * For generating the keys see:
 *  https://github.com/web-push-libs/web-push-php#authentication-vapid
 */
define('WEBPUSH_PUBLIC_KEY', '{{ webpush_public_key }}');
define('WEBPUSH_PRIVATE_KEY', '{{ webpush_private_key }}');

// # Twingle URL for caching donation campaign
define('TWINGLE_URL', '{{ twingle_donation_status_url }}');

// # Geoapify map tiles
define('GEOAPIFY_API_KEY', 'b4e6bf0dbc48447fb4ee29d77c08eb09');

// # Monitoring and Reporting
define('SENTRY_URL', '{{ sentry_url_beta }}');
define('SENTRY_TRACING_SAMPLE_RATE', 0.2);
define('RAVEN_JAVASCRIPT_CONFIG', '{{ sentry_raven_javascript_config_beta }}');

define('CSP_REPORT_URI', '{{ sentry_csp_report_beta }}');
define('CSP_REPORT_ONLY', false);


// # BigBlueButton
define('BBB_DOMAIN', 'bbb.foodsharing.network');
define('BBB_SECRET', '{{ bbb_secret }}');
define('BBB_DIALIN', '+49 5563 263 9980 (Deutschland) oder +49 3425 9999 000 (Deutschland) oder +41 3251 23 23 0 (Schweiz)');

/*
 * the following cache settings are an _example_.
 * Caching should have no or only minimum impact on the displayed content (e.g. some seconds delay/ttl is acceptable for most content, but should only be used when there is a reason).
 * caching indexes:
* u = cache page for logged in users
* g = cache page for guest
* value is time to live for the cache...
*/
$both = array('u' => 3600, 'g' => 3600);
$user = array('u' => 3600);
$guests = array('g' => 3600);
$both_short = array('u' => 60, 'g' => 60);
$user_short = array('u' => 60);

$g_page_cache = array(
);